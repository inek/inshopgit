from django.contrib import admin

# Register your models here.

from .models import Product, Shop, Category, Order, Customer

admin.site.register((Product, Shop, Category, Order, Customer))
