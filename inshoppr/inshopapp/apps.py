from django.apps import AppConfig


class InshopappConfig(AppConfig):
    name = 'inshopapp'
