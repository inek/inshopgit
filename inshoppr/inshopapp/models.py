from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class Customer(models.Model):
    name = models.TextField(max_length=20, default="Mike")
    surname = models.TextField(max_length=20, default="Tyson")
    email = models.EmailField(max_length=30, default="@")
    address = models.CharField(max_length=99)
    phone = models.TextField(max_length=12, default=375)
    note = models.TextField

    def __str__(self):
        return '{0.name} {0.surname} {0.email} {0.address} {0.phone} {0.note}'.format(self)

    class Meta:
        verbose_name = 'Customerz'
        verbose_name_plural = 'Customerzzz'


class Shop(models.Model):
    name = models.CharField(max_length=128)

    def get_absolute_url(self):
        return 'http://iluser.pythonanywhere.com/'


class Category(models.Model):
    name = models.CharField(max_length=256)
    parent_category = models.IntegerField(default=0)
    meta = models.CharField(max_length=256)

    def __str__(self):  # #
        return self.name    # #
        # return str(self.pk)


class Product(models.Model):
    name = models.CharField(max_length=256)
    price = models.IntegerField()
    meta = models.TextField()
    category = models.ManyToManyField(Category)

    def __str__(self):
        # return self.name
        return 'name {}'.format(self.name)


class Order(models.Model):
    product = models.ForeignKey(Product, related_name='order')
    user = models.ForeignKey(User)
    created_at = models.DateTimeField(auto_now=True)
    count = models.IntegerField(default=1)



