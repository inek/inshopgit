from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class TestModel(models.Model):
    test_field1 = models.CharField(max_length=256)
    test_field2 = models.IntegerField(default=4)
    test_field3 = models.TextField()

    def __str__(self):
        return '{0.test_field1} {0.test_field2} {0.test_field3}'.format(self)


class Shop(models.Model):
    name = models.CharField(max_length=128)

    def get_absolute_url(self):
        return 'https://tut.by'


class Category(models.Model):
    name = models.CharField(max_length=256)
    parent_category = models.IntegerField(default=0)
    meta = models.CharField(max_length=256)

    def __str__(self):  # #
        return self.name    # #
        # return str(self.pk)


class Product(models.Model):
    name = models.CharField(max_length=256)
    price = models.IntegerField()
    meta = models.TextField()
    category = models.ManyToManyField(Category)

    def __str__(self):
        # return self.name
        return 'name {}'.format(self.name)


class Order(models.Model):
    # product = models.ForeignKey(Product)
    product = models.ForeignKey(Product, related_name='order')
    user = models.ForeignKey(User)
    created_at = models.DateTimeField(auto_now=True)
    count = models.IntegerField(default=1)

# class Blog(models.Model):
#     name = models.CharField()
#
#
# class Entry(models.Model):
#     blog = models.ForeignKey(Blog)

